#!/usr/bin/env node

import fs from "fs";
import { Command } from "commander";
import initModCommand from "./command/modCommand.mjs";
import initCbaCommand from "./command/cbaCommand.mjs";
import initGSMConfigCommand from "./command/gsmConfigCommand.mjs";

// Init File For first time
if (!fs.existsSync("./.information.json")) {
  fs.writeFileSync("./.information.json", "{}");
}

const program = new Command();

program
  .name("cli-update-arma3")
  .description("A CLI tool for manage Arma 3 Mods and Server")
  .version("1.0.0");

// Commande d'update des mod

initModCommand(program);
initCbaCommand(program);
initGSMConfigCommand(program);
program.parse();

/*---------------------------------------------------------------------------------*/
