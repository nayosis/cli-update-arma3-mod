#!/bin/bash
depth=0
echo "rename $1"

for x in $(find $1 -type d | sed "s/[^/]//g")
do
if [ ${depth} -lt ${#x} ]
then
   let depth=${#x}
fi
done
echo "the depth is ${depth}"
for ((i=1;i<=${depth};i++))
do
  for x in $(find $1 -maxdepth $i | grep [A-Z])
  do
    echo "rename ..  $x"
    mv $x $(echo $x | tr 'A-Z' 'a-z')
  done
done
