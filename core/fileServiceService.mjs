import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import cp from "child_process";
import chalk from "chalk";

const A3_WORKSHOP_ID = "107410";

const WORKSHOP_DIR = process.env.WORKSHOP_DIR;
const ARMA3_DIR = process.env.ARMA3_DIR;

//     /home/arma3server/mods/steamapps/workshop/content/107410
const WORKSHOP_DIR_EFFECTIVE_CONTENT = `${WORKSHOP_DIR}/steamapps/workshop/content/${A3_WORKSHOP_ID}`;
const ARMA3_DIR_MOD_EFFECTIVE_CONTENT = `${ARMA3_DIR}/mod`;

// /home/arma3server/serverfiles/mod

function creerSymlink(id) {
  console.log(chalk.greenBright(" Création du lien symbolique"));
  // Creet un symlink depuis le workshop vers le lien de mod
  // - id mod
  // - source link

  const source = `${WORKSHOP_DIR_EFFECTIVE_CONTENT}/${id}`;
  const target = `${ARMA3_DIR_MOD_EFFECTIVE_CONTENT}/${id}`;

  // - target link
  if (!fs.existsSync(target)) {
    fs.symlinkSync(source, target, "dir");
    console.log(chalk.greenBright(" OK "));
  } else {
    console.log(chalk.bgGray(" Le lien exist deja ! "));
  }
}

function lowerCase(id) {
  const source = `${WORKSHOP_DIR_EFFECTIVE_CONTENT}/${id}`;
  const buffer = cp.execSync(`./dependancies/rename_files.sh ${source}`);

  const recap = buffer.toString();
  console.log(recap);
}

const fileServiceService = {
  creerSymlink: creerSymlink,
  lowerCase: lowerCase,
};

export default fileServiceService;
