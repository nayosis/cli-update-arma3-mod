import chalk from "chalk";

import axios from "axios";

import * as dotenv from "dotenv";
dotenv.config();

import { execSync } from "child_process";

const STEAM_USER = process.env.STEAM_USER;
const STEAM_PASS = process.env.STEAM_PASS;
const STEAM_TOKEN = process.env.STEAM_TOKEN;
const A3_WORKSHOP_ID = "107410";

const WORKSHOP_DIR = process.env.WORKSHOP_DIR;
const STEAM_CMD_DIR = process.env.STEAM_CMD_DIR;

const STEAM_IPUBLISH_API = `https://api.steampowered.com/IPublishedFileService/GetDetails/v1/?key=${STEAM_TOKEN}&publishedfileids[0]=`;

/**
 * Download DDL
 * 0 no error
 * 1 erreur lors du dl
 * 2 mauvais configuration
 * @param {*} mod
 * @returns status (0 no error ,1 error)
 */
async function update(mod) {
  // Construction de la commande STEAM
  let steam_cmd_params;
  steam_cmd_params = ` +force_install_dir ${WORKSHOP_DIR}`;
  steam_cmd_params += ` +login ${STEAM_USER} ${STEAM_PASS}`;
  steam_cmd_params += ` +workshop_download_item ${A3_WORKSHOP_ID} ${mod.wksid} validate`;
  steam_cmd_params += " +quit";

  console.log("Execution de steam CMD ");
  console.log(steam_cmd_params);

  const buffer = execSync(`${STEAM_CMD_DIR} ${steam_cmd_params}`);

  const recap = buffer.toString();
  console.log(chalk.grey(recap));
  if (recap.includes("Success")) {
    return 0;
  }
  return 9;
}

async function readInformations(id) {
  return await axios.get(`${STEAM_IPUBLISH_API}${id}`).then((response) => {
    const modINfo = response.data?.response?.publishedfiledetails[0];

    if (modINfo) {
      return {
        timeCreated: modINfo.time_created,
        timeUpdated: modINfo.time_updated,
        title: modINfo.title,
        appid: modINfo.creator_appid,
      };
    }
    return undefined;
  });
}

/**
 * SERVICE D'INTERACTION avec le stamWorkshop
 */
const workShopService = {
  update: update,
  readInformations: readInformations,
};

export default workShopService;
