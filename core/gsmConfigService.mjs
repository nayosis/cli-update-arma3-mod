import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import chalk from "chalk";
import modService from "./modService.mjs";

const GSM_CONFIG_INSTANCE_FILE = process.env.GSM_CONFIG_INSTANCE_FILE;

function loadModConfig(listModName = undefined) {
  const modsArg = buildModParameterString(listModName);
  console.info(chalk.bgBlueBright(`Argument des mods : `));
  console.info(chalk.grey(modsArg));
  writeConfigFile(modsArg);
}

function writeConfigFile(listLine) {
  if (GSM_CONFIG_INSTANCE_FILE === undefined) {
    throw new Error(
      "GSM_CONFIG_INSTANCE_FILE is not DEFINED. Please check envFile"
    );
  }
  fs.writeFileSync(GSM_CONFIG_INSTANCE_FILE, listLine);
}

function buildModParameterString(listName = undefined) {
  console.log(listName);
  const allMods = modService
    .readAll()
    .filter((mod) => listName === undefined || listName.includes(mod.name));
  return allMods
    .filter((mod) => mod.activ)
    .reduce((agr, mod) => {
      agr = agr + `mod/${mod.wksid}\\;`;
      return agr;
    }, "mods=");
}

const gsmConfigService = { loadModConfig };

export default gsmConfigService;
