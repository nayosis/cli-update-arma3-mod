import * as dotenv from "dotenv";
import fs from "fs";
import path from "path";
dotenv.config();

import axios from "axios";
import FormData from "form-data";

const DISCORD_WEBHOOK = process.env.DISCORD_WEBHOOK;

const avatarurl = "https://units.arma3.com/groups/img/76317/juFZQrmDjk.png";

function sentmessageFields(username, fields) {
  const body = {
    username: username,
    avatar_url: avatarurl,
    embeds: [
      {
        fields: fields,
      },
    ],
  };
  axios.post(`${DISCORD_WEBHOOK}`, body).then((response) => {
    //console.log(response.statusText);
  });
}

function sendMessage(username, content) {
  const body = { username: username, avatar_url: avatarurl, content: content };
  //console.log(DISCORD_WEBHOOK);
  axios.post(`${DISCORD_WEBHOOK}`, body).then((response) => {
    //console.log(response.statusText);
  });
}

function sendMessageWithFile(username, content, fileContent, fileName) {
  const form = new FormData();

  const body = { username: username, avatar_url: avatarurl, content: content };

  fs.writeFileSync("./tempo", fileContent);
  const file = fs.readFileSync("./tempo"); //"./mods.json"
  form.append("file", file, fileName);

  form.append("payload_json", JSON.stringify(body));

  ("Content-Type: application/json");
  axios.post(`${DISCORD_WEBHOOK}`, form, {
    headers: {
      "content-type": "multipart/form-data",
    },
  });
}

function notifyUpdate(mod, informations, statusExec) {
  let message;
  switch (statusExec) {
    case 0:
    case "0":
      message = `
  ---------------------------------------------------------------
  :up: Mise à jour du Mod ${informations.title}. 
  :postal_horn: Version ${informations.timeUpdated} 
  ---------------------------------------------------------------
              `;
      break;
    default:
      message = `
  ---------------------------------------------------------------
  :up: Erreur lors de la mise jour du Mod ${informations.title}. 
  :postal_horn: Version ${informations.timeUpdated}
  ---------------------------------------------------------------
              `;
  }

  sendMessage("Arma Server Tools", message);
}

const eventService = {
  notifyUpdate,
  sendMessage,
  sendMessageWithFile,
  sentmessageFields,
};

export default eventService;

/*

{
  "embeds": [{
    "fields": [
      {
        "name": "Cat",
        "value": "Hi! :wave:",
        "inline": true
      },
      {
        "name": "Dog",
        "value": "hello!",
        "inline": true
      },
      {
        "name": "Cat",
        "value": "wanna play? join to voice channel!"
      },
      {
        "name": "Dog",
        "value": "yay"
      }
    ]
  }]
}

*/
