import fs from "fs";

/**
 * Add a mod to datastore
 * @param {string} wksid unique identifier workshopMod
 * @param {string} name name of mod
 * @returns
 */
function addMod(wksid, name) {
  if (readMod(wksid)) {
    console.error("Already exist", wksid, name);
    return "";
  }
  const newModData = {
    name: name,
    wksid: wksid,
    activ: false,
    type: "steam",
  };

  return upsertMod(newModData);
}

/**
 * remove a mod from dataStore
 * @param {string} wksid unique identifier workshopMod
 * @returns
 */
function removeMod(wksid) {
  let mods = readAllMod();
  const newmods = mods.filter((mod) => mod.wksid !== wksid);
  return writeData(newmods);
}

function readAllMod() {
  return readData();
}

function readMod(wksid) {
  return readAllMod().find((mod) => mod.wksid === wksid);
}

function upsertMod(mod) {
  const allMod = readAllMod().filter(
    (innerMod) => innerMod.wksid !== mod.wksid
  );
  allMod.push(mod);
  return writeData([...allMod]);
}

function activateMod(wksid) {
  const mod = readMod(wksid);
  mod.activ = true;
  return upsertMod({ ...mod });
}

function deactivateMod(wksid) {
  const mod = readMod(wksid);
  mod.activ = false;
  return upsertMod({ ...mod });
}

const modService = {
  readAll: readAllMod,
  read: readMod,
  add: addMod,
  delete: removeMod,
  activate: activateMod,
  desactivate: deactivateMod,
};

export default modService;

function writeData(mods) {
  const raw = JSON.stringify(mods, null, 2);
  fs.writeFileSync("./mods.json", raw);
  return "done";
}

function readData() {
  if (!fs.existsSync("./mods.json")) {
    return [];
  }
  const rawdata = fs.readFileSync("./mods.json");
  let mods = JSON.parse(rawdata);
  return mods;
}
