import fs from "fs";

/**
 * Permet de mettre à jour les meta données d'un mod dans le .information.json
 * @param {*} id identifiant du mod
 * @param {*} version version du mod
 * @param {*} error erreur lors de la maj du mod
 */
function touchModMeta(id, version, error) {
  const raw = fs.readFileSync("./.information.json");
  let modMetadata = JSON.parse(raw);
  modMetadata[id] = { date: new Date(), version: version, error: error };

  fs.writeFileSync("./.information.json", JSON.stringify(modMetadata, null, 2));
}

/**
 * permet de lire les meta information du mod
 * @param {*} id identifiant du mod
 * @returns les meta infomation du mod
 */
function readModMeta(id) {
  const raw = fs.readFileSync("./.information.json");
  let modMetadata = JSON.parse(raw);
  return modMetadata[id];
}

/**
 * SERVICE D'INTERACTION AVEC LES META
 */
const metaInformationsService = {
  touchModMeta: touchModMeta,
  readModMeta: readModMeta,
};

export default metaInformationsService;
