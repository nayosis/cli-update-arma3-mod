import fs from "fs";
import path from "path";

import * as dotenv from "dotenv";
dotenv.config();
import eventService from "./eventService.mjs";

const USERCONFIG_DIR = process.env.USERCONFIG_DIR;

const DEFAULTFILENAME = "cba_settings.sqf";

const cbaConfigService = {
  list: listConfig,
  search: searchConfig,
  use: useConfig,
};

export default cbaConfigService;

function listConfig() {
  // lister les fichier du dossier de config CBA
  const files = fs.readdirSync(USERCONFIG_DIR);
  files.forEach((file) => {
    if (file?.includes(".tsqf")) {
      console.log("config", file);
    }
  });
}

function searchConfig(templateName) {
  // lister les fichier du dossier de config CBA
  const files = fs.readdirSync(USERCONFIG_DIR);
  const file = files.find((file) => {
    if (file?.includes(".tsqf")) {
      if (file.split(".")[0] === templateName) {
        return true;
      }
    }
    return false;
  });
  return file;
}

function useConfig(templateName) {
  // lister les fichier du dossier de config CBA
  const file = searchConfig(templateName);
  if (file) {
    //remove precedent
    if (fs.existsSync(`${USERCONFIG_DIR}${path.sep}${DEFAULTFILENAME}`)) {
      fs.rmSync(`${USERCONFIG_DIR}${path.sep}${DEFAULTFILENAME}`);
    }

    // create new
    fs.copyFileSync(
      `${USERCONFIG_DIR}${path.sep}${templateName}.tsqf`,
      `${USERCONFIG_DIR}${path.sep}${DEFAULTFILENAME}`
    );

    let message = `
    ---------------------------------------------------------------
    :up: Changement de la configuration CBA 
     *Template utilisé :"${path.sep}${templateName}". 
    ---------------------------------------------------------------
                `;

    eventService.sendMessage("Configurateur CBA", message);
  }
}
