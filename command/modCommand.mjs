// init commande
import emoji from "node-emoji";
import chalk from "chalk";
import Table from "easy-table";

import modService from "../core/modService.mjs";
import workShopService from "../core/steamWorkShop.mjs";
import eventService from "../core/eventService.mjs";
import metaInformationsService from "../core/metaInformation.mjs";
import fileServiceService from "../core/fileServiceService.mjs";
// Commande de gestion de mod
export default function initModCommand(program) {
  const modCommand = program.command("mod").description("Gestion des mods");

  modCommand
    .command("update")
    .description("Update one mod")
    .argument("<unique Id>", "Identifiant Unique du mod")
    .option("-f, --force", "force", false)
    .action((id, options) => {
      majMod(id, options.force);
    });

  modCommand
    .command("update-all")
    .description("Update all mod if necessary")
    .action((a) => {
      majAllMods();
    });

  modCommand
    .command("add")
    .description("Add new mod")
    .argument("<unique Id>", "Identifiant Unique du mod")
    .argument(
      "<uniqueName>",
      "Name of the mod (lowercase with noSpace and @ at first"
    )
    .action((wkid, name) => {
      addMod(wkid, name);
    });

  modCommand
    .command("remove")
    .description("Remove mod")
    .argument("<unique Id>", "Identifiant Unique du mod")

    .action((wkid) => {
      removeMod(wkid);
    });

  modCommand
    .command("activate")
    .description("Activate")
    .argument("<unique Id>", "Identifiant Unique du mod")
    .requiredOption("-s --status <boolean>", " status demande", true)
    .action((wkid, options) => {
      if (!["true", "false"].includes(options.status)) {
        console.error(
          chalk.red("L'argument -s doit être suivie de true|false")
        );
        return;
      }
      if (options.status === "true") {
        modService.activate(wkid, options.status);
      } else {
        modService.desactivate(wkid, options.status);
      }
    });

  // Commande d'information

  modCommand
    .command("version")
    .description("Read the last Version available")
    .argument("<unique Id>", "Identifiant Unique du mod")
    .action(async (a) => {
      console.log(await workShopService.readInformations(a));
    });

  modCommand
    .command("symLink")
    .description("Symlink (technical)")
    .argument("<unique Id>", "Identifiant Unique du mod")
    .action(async (id) => {
      console.log(await fileServiceService.creerSymlink(id));
    });

  modCommand
    .command("lowerCase")
    .description("lowerCase (technical)")
    .argument("<unique Id>", "Identifiant Unique du mod")
    .action(async (id) => {
      console.log(await fileServiceService.lowerCase(id));
    });

  modCommand
    .command("list")
    .description("lister les mods")
    .action(() => {
      displayMod(true).then((values) => {
        console.log(Table.print(values));
      });
    });

  modCommand
    .command("publish")
    .description("Publish list mod to Discord")
    .action(() => {
      displayMod().then((strings) => {
        eventService.sendMessageWithFile(
          "WDG Center Operation",
          "Liste des mods du server",
          //Table.print(strings),
          Table.print(strings),
          "mods.txt"
        );
      });
    });
}

/**
 * Maj tous les mods
 * Cycle sur la liste des mods
 */
async function majAllMods() {
  const mods = modService.readAll();
  for (let index = 0; index < mods.length; index++) {
    const mod = mods[index];
    await majMod(mod.wksid);
    console.log(chalk.white(""));
  }
}

/**
 * Mets à jour un mod par son ID
 * Doit être referencer dans la liste des mods
 * @param {*} id WorkShopId Mod
 * @param {*} force force Update
 * @returns
 */
async function majMod(id, force = false) {
  console.log(chalk.white("###############################################"));
  const mod = modService.read(id);
  if (mod === undefined) {
    console.log(
      chalk.red(
        `Le mod ${id}, n'est pas présent dans le fichier de configuration des mods`
      )
    );
    console.log(
      chalk.whiteBright(" Pour l'ajouter, regarder la commande <add> ")
    );
    console.log(chalk.white("###############################################"));
    return;
  }

  console.log(
    chalk.white("Vérification d'une nouvelle version pour le mod " + mod.name)
  );
  const modMeta = metaInformationsService.readModMeta(mod.wksid);
  const workShopInformations = await workShopService.readInformations(
    mod.wksid
  );

  const sameVersion = modMeta?.version === workShopInformations.timeUpdated;

  if (sameVersion && !force) {
    console.log(chalk.green(mod.name + " est deja à jour ! "));
    console.log(chalk.white("###############################################"));
  } else {
    console.log(chalk.bgGreenBright(mod.name + " va être mis à jour ! "));

    console.log(chalk.white("Initilisation du téléchargement "));
    let statusExec;
    switch (mod.type) {
      case "steam":
        statusExec = await workShopService.update(mod);
        break;
      default:
        statusExec = "UNKNOW";
    }

    console.log(chalk.white(`Fin du téléchargement  key:(${statusExec})`));

    console.log(chalk.green("Mise à jour des métadata"));
    eventService.notifyUpdate(mod, workShopInformations, statusExec);
    switch (statusExec) {
      case 0:
      case "0":
        metaInformationsService.touchModMeta(
          mod.wksid,
          workShopInformations.timeUpdated,
          undefined
        );
        break;
      default:
        metaInformationsService.touchModMeta(mod.wksid, undefined, statusExec);
    }
  }
  fileServiceService.creerSymlink(mod.wksid);
  fileServiceService.lowerCase(mod.wksid);
  console.log(chalk.white("###############################################"));
}

/**
 * Affiche la liste des mods configurer
 */
function displayMod() {
  const view = modService
    .readAll()
    .sort((a, b) => a.wksid - b.wksid)
    .map(async (mod) => {
      const modMeta = metaInformationsService.readModMeta(mod.wksid);
      const information = await workShopService.readInformations(mod.wksid);

      return {
        mod: mod.name,
        id: mod.wksid,
        actif: mod.activ
          ? emoji.get("white_check_mark")
          : emoji.get("exclamation"),
        date: modMeta?.date,
        version: modMeta?.version,
        lastVersion: information.timeUpdated,
        ok:
          information.timeUpdated === modMeta?.version
            ? emoji.get("white_check_mark")
            : emoji.get("exclamation"),
        error: modMeta?.error,
      };
    });
  return Promise.all(view);
}

/**
 * add mod
 * @param {*} wkid
 * @param {*} name
 * @returns
 */
async function addMod(id, name) {
  // ajour d'un mod dans la liste
  if ((await workShopService.readInformations(id)).appid !== 107410) {
    console.error(
      chalk.bgRedBright(
        `Le mod avec l'ID <${id}> ne correspond pas à un mod existant pour arma 3`
      )
    );
    return;
  }
  modService.add(wkid, name);
}

/**
 * supprime le mod de la liste
 * @param {*} id
 */
async function removeMod(id) {
  modService.delete(id);
}

function buildModParameterString() {
  const allMods = modService.readAll();

  return allMods
    .filter((mod) => mod.activ)
    .reduce((agr, mod) => {
      agr = agr + `mod/${mod.wksid}\\;`;
      return agr;
    }, "-mod=");
  //-mod=mods/@anizay\;mods/@niakala\;mods/@zeus_enhanced\;mods/@zeus_enhanced_ac3_comp\;mods/@cba_a3\;mods/@cup_ace_terrains\;mods/@cup_ace_vehicles\;mods/@cup_ace_weapons\;mods/@cup_terrains_core\;mods/@cup_unit\;mods/@cup_vehicles\;mods/@taskforce\;mods/@cup_weapons\;mods/@dui_squad_radar\;mods/@enhanced_movement\;mods/@enhanced_movement_rework\;mods/@gge_captive_animation\;mods/@gge_core\;mods/@kujari\;mods/@pulau\;mods/@simplex_support_service\;mods/@vt7\;mods/@ace\;mods/@dzn_artillery_illumination\;mods/@lambs_danger\;mods/@zeus_additions\;mods/@pir\;mods/@pir_acemedcompat\;mods/@zlusken_hdcs\;mods/@dynamic_camo_system\;"
}
