import cbaConfigService from "../core/cbaConfigService.mjs";

export default function initCbaCommand(program) {
  const cba = program
    .command("cba")
    .description("Gestion des config CBA (Beta)");

  cba
    .command("list")
    .description("list all config file")
    .action(() => {
      cbaConfigService.list();
    });

  cba
    .command("search <templateName>")
    .description("search configFile ")
    .action((templateName) => {
      console.log(cbaConfigService.search(templateName));
    });

  cba
    .command("use <templateName>")
    .description("Use configFile ")
    .action((templateName) => {
      console.log(cbaConfigService.use(templateName));
    });

  cba
    .command("create")
    .description("docs Create")
    .action(() => {
      console.log(
        `
NOT YET IMPLEMENTED
Documentation
          `
      );

      console.log(
        `
Une config est un fichier tsqf ( template SQF). 
    il suffit donc d'upload sur le serveur dans le dossier adequeta( userconfig (de cba) )
    un fichier avec vos conf. Les commands vous permettent de changer la conf avec un commande
      `
      );
    });
}
