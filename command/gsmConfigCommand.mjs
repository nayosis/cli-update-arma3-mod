import gsmConfigService from "../core/gsmConfigService.mjs";
import modService from "../core/modService.mjs";
import enquirer from "enquirer";
const { MultiSelect } = enquirer;

export default function initGSMConfigCommand(program) {
  const listChoice = modService.readAll().map((mod) => {
    return {
      name: mod.name,
      value: mod.wksid,
      hint: mod.activ,
    };
  });

  const defaultListChoice = modService
    .readAll()
    .filter((mod) => mod.activ)
    .map((mod) => {
      return mod.name;
    });

  const gsm = program
    .command("gsm")
    .description("Gestion des config GSM (Beta)");

  gsm
    .command("load-all")
    .description("Load all default mod")
    .action(() => {
      gsmConfigService.loadModConfig();
    });
  gsm
    .command("load")
    .description("Load selected mod ")
    .action(async () => {
      const prompt = new MultiSelect({
        name: "List des mods ( parameter command startup arma 3)",
        message: "selectionner les mod à activer sur le serveur : ",
        initial: defaultListChoice,
        choices: listChoice,
      });

      prompt
        .run()
        .then((answers) => gsmConfigService.loadModConfig(answers))
        .catch(console.error);
    });
}
